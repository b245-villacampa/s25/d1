console.log("hello world")

// [SECTION] JSON OBJECTS
	// JSON stamds for JavaScript Object Notation
	// JSON is also used in other programming languages
	//core Javasriptr has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript Objects
		// JSON object -  it means PARSED!
		//Stringified JSON Object - JSON Objecty na nakastringify
	//JSON is used for serializing/converting different data types
	/*
		Syntax/Format:
			
			{
				"propertyA" : "valueA",
				"propertyB" : "valueB"
			}

	*/ 

	// {
	// 	"city" : "Quezon City",
	// 	"province" : "Manila",
	// 	"country" : "Philippines"
	// }

	// JSON ARRAY
		// Array of JSON Object
	// [
		// 	{
		// 		"city" : "Manila City",
		// 		"province" : "Manila",
		// 		"country" : "Philippines"
		// 	}
	// ]

	//JSON METHODS 
		//JSON methods contains method for parsing and converting data info stringified JSON 

		let batchesArr = [
			{
				batchName : "Batch X"
			},
			{
				batchName : "Batch Y"

			}
		];

		console.log("This is the orifginal array:");
		console.log(batchesArr);

		// The stringify method is used to convert JavaScript Objeccts into string

		let stringBatchesArr = JSON.stringify(batchesArr);

		console.log("This is the result of stringify method: ")
		console.log(stringBatchesArr);
		console.log("Data type:");
		// To check the data type of the array after stringify method
		console.log(typeof stringBatchesArr);

		console.log("This is the original array of objects:")
		console.log(batchesArr);

		let data = JSON.stringify({
			name: "John",
			address :{
				city : "Manila",
				country: "Philippines"
			}
		});

		console.log(data);

	// USE STRINGIFY METHOD WITH VARIABLES
		// when information is stored is a variable and is not hard coded into an onfect yhat is being stringified, we can supply value with a variable;

		let firstName = "Lebron";
		let lastName = "James";
		let age = 38;
		let address = {
			city : "Manila City",
			country : "PH"
		}

		let otherData = {
			firstName,
			lastName,
			age,
			address
		}

		console.log(otherData);

		// CONVERTING STRINGIFIED JSON INTO JAVASCRIPT OBJECTS
			/*
				- Objects are common data types used in application because of the comples data structure that can be created out of them.
				
				- Information is commonly sent to application in stringified JSON and then converted back into objects
				
				-This happens both for sending information to a backend application and spending information back to frontend application

			*/

		// Parse method converts stringified JSON into JSON objects;
		let objectBatchesArr = JSON.parse(stringBatchesArr);
		console.log("This is the stringified version:")
		console.log(stringBatchesArr);
		console.log("This is the result after the p[arse method:");
		console.log(objectBatchesArr);
		console.log(typeof objectBatchesArr);
		console.log(objectBatchesArr[0]);


		let stringifiedObject = `{
			"name" : "John",
			"age"  : "31",
			"adress":{
				"city":"Manila",
				"country":"Phillipines"
			}
		}`;

		console.log(JSON.parse(stringifiedObject));